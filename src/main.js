import Vue from 'vue';
import App from './Index.vue';
import VueRouter from 'vue-router';
import  { routes } from './routes';
import BootstrapVue from 'bootstrap-vue';
import VueResource from 'vue-resource';
import VModal from 'vue-js-modal';
import LoadScript from 'vue-plugin-load-script';
import vQuery from 'vue-jquery';
import cors from 'cors';
Vue.use(vQuery);
// eslint-disable-line no-undef no-unused-vars
import jQuery from 'jquery';
global.jQuery = jQuery;
//eslint-disable-next-line
let Bootstrap = require('bootstrap');
import './vendor/bootstrap/css/bootstrap.css';

//require('./js/req.js');
Vue.use(VueResource);
Vue.use(VueRouter) ;
Vue.use(BootstrapVue) ;
Vue.use(VModal) ;
Vue.use(LoadScript);
Vue.use(cors)


Vue.config.productionTip = false

const router =  new VueRouter({
  routes,
  mode:'hash'
});

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
