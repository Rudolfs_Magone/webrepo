
import Home from './Home.vue';
import Header from './Header.vue';
import Dzemperi from './components/Dzemperi.vue';
import Krekli from './components/Krekli.vue';
import Zekes from './components/Zekes.vue';
import Mauci from './components/Mauci.vue';
import Search from './components/Search.vue';

export const routes=[
    {path: '',component: Home},
    {path: '/dzemperi/:type',component: Dzemperi},
    {path: '/mauci/:type',component: Mauci},
    {path: '/zekes/:type',component: Zekes},
    {path: '/krekli/:type',component: Krekli},
    {path: '/search/:value',component: Search},
];
